
object TrafficLightColor extends Enumeration {
  type TrafficLightColor = Value
  val Red, Yellow, Green = Value
}

object Accounts {
  def apply(balance: Double) = new Accounts(balance)
}

class Accounts(val balance: Double) {

}
import TrafficLightColor._

object ChapterSix extends App {
  println(TrafficLightColor.values)
  for (v: TrafficLightColor <- TrafficLightColor.values)
    println(v)
}
