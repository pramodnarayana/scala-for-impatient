import java.util.{Calendar, Properties}

import scala.collection.JavaConversions.mapAsScalaMap
import scala.collection.immutable.TreeMap
import scala.collection.mutable
import scala.io.Source

object ChapterFour {
  def main(args: Array[String]) {
    new ChapterFour().ten()
  }
}

class ChapterFour {
  def two() = {
    val source = Source.fromFile("/Volumes/Data/tmp/scala-test/src/main/resources/two_two.txt")
    val lineIterator = source.getLines
    val frequency: scala.collection.mutable.HashMap[String, Int] = scala.collection.mutable.HashMap[String, Int]()

    for (line <- lineIterator)
      line.split("\\s").map(word => frequency(word) = frequency.getOrElse(word, 0) + 1)
    source.close

    println(frequency.mkString(" "))
  }

  def frequency(map: Map[String, Int]) = {
    val source = Source.fromFile("/Volumes/Data/tmp/scala-test/src/main/resources/two_two.txt")
    val lineIterator = source.getLines

    def iterateOverWords(splitted: Array[String], innerFrequency: Map[String, Int]): Map[String, Int] = {
      if (!splitted.isEmpty) {
        iterateOverWords(splitted.tail, innerFrequency + (splitted.head -> (innerFrequency.getOrElse(splitted.head, 0) + 1)))
      } else {
        innerFrequency
      }
    }

    def iterateOverLines(frequency: Map[String, Int]): Map[String, Int] = {
      if (lineIterator.hasNext) {
        iterateOverWords(lineIterator.next.split("\\s"), frequency)
      } else {
        frequency
      }
    }
    iterateOverLines(map)
  }

  def three_four_five() = {
    val treeMap: TreeMap[String, Int] = scala.collection.immutable.TreeMap[String, Int]() ++ new java.util.TreeMap[String, Int]()

    println("Three = " + frequency(scala.collection.immutable.HashMap[String, Int]()))
    println("Four = " + frequency(TreeMap[String, Int]()))
    println("Five = " + frequency(treeMap))
  }

  def six() = {
    val stringToCalendar: mutable.LinkedHashMap[String, Int] = new mutable.LinkedHashMap[String, Int]()
    stringToCalendar += ("SUNDAY" -> Calendar.SUNDAY)
    stringToCalendar += ("MONDAY" -> Calendar.MONDAY)
    stringToCalendar += ("TUESDAY" -> Calendar.TUESDAY)
    stringToCalendar += ("WEDNESDAY" -> Calendar.WEDNESDAY)
    stringToCalendar += ("THURSDAY" -> Calendar.THURSDAY)
    stringToCalendar += ("FRIDAY" -> Calendar.FRIDAY)
    stringToCalendar += ("SATURDAY" -> Calendar.SATURDAY)
    for ((k, v) <- stringToCalendar) {
      println(k + " = " + v)
    }
  }

  def seven() = {
    val properties: Properties = System.getProperties
    val biggestKey: Int = getBiggestLength(properties.iterator, 0, (k, v) => k)
    val biggestValue: Int = getBiggestLength(properties.iterator, 0, (k, v) => v)

    println("-" * (biggestKey + biggestValue + 3))
    for ((k: String, v: String) <- properties) {
      print(k)
      print(" " * Math.abs(biggestKey - k.length))
      print(" | ")
      println(v)
    }
    println("-" * (biggestKey + biggestValue + 3))
  }

  def getBiggestLength(properties: Iterator[(AnyRef, AnyRef)], biggestLength: Int, getKeyOrValue: ((AnyRef, AnyRef) => AnyRef)): Int = {
    if (properties.hasNext) {
      val next: (AnyRef, AnyRef) = properties.next
      val keyOrValue: AnyRef = getKeyOrValue(next._1, next._2)
      keyOrValue match {
        case x: String =>
          if (x.length > biggestLength) getBiggestLength(properties, x.length, getKeyOrValue) else getBiggestLength(properties, biggestLength, getKeyOrValue)
      }
    } else {
      biggestLength
    }
  }

  def eight() = {
    val minMax: (Int, Int) = (0, 0)
    val values: Array[Int] = Array[Int](-1, 1, 2, 3, 4, 5)

    def recursiveIter(array: Array[Int]): (Int, Int) = {
      if (array.isEmpty) {
        (0, 0)
      } else {
        val previousIteration: (Int, Int) = recursiveIter(array.tail)
        if (array.head < previousIteration._1) (array.head, previousIteration._2)
        else if (array.head > previousIteration._2) (previousIteration._1, array.head)
        else previousIteration
      }
    }

    def tailRecursive(array: Array[Int], minMax: (Int, Int)): (Int, Int) = {
      if (array.isEmpty)
        minMax
      else {
        if (array.head < minMax._1)
          tailRecursive(array.tail, (array.head, minMax._2))
        else if (array.head > minMax._2)
          tailRecursive(array.tail, (minMax._1, array.head))
        else
          throw new Error
      }
    }
    println(recursiveIter(values))
    println(tailRecursive(values, (0, 0)))
  }

  def nine() = {
    def iter(arr: Array[Int], v: Int, acc: (Int, Int, Int)): (Int, Int, Int) = {
      if (arr.isEmpty)
        acc
      else if (arr.head < v)
        iter(arr.tail, v, (acc._1 + 1, acc._2, acc._3))
      else if (arr.head > v)
        iter(arr.tail, v, (acc._1, acc._2, acc._3 + 1))
      else
        iter(arr.tail, v, (acc._1, acc._2 + 1, acc._3))
    }

    println(iter(Array[Int](1, 2, 3, 4, 5), 3, (0, 0, 0)))
    println(iter(Array.empty[Int], 3, (0, 0, 0)))
  }

  def ten() = {
    println("Hello".zip("World"))
    println("Hello".zip("World1"))
    println("Hello1".zip("Wo"))
  }
}
