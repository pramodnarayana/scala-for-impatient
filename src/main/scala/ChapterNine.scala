import java.io._

import scala.collection.mutable.Stack
import scala.io.Source

object ChapterNine extends App {
  ten

  def one = {
    var stack = new Stack[String]()
    val in = Source.fromFile("src/main/resources/9/1.txt", "UTF-8")
    val linesIterator: Iterator[String] = in.getLines()
    for (line <- linesIterator)
      stack.push(line)

    val out: PrintWriter = new PrintWriter("src/main/resources/9/1_out.txt", "UTF-8")
    for (elem <- stack)
      out.write(elem + "\n")
    out.close()
  }

  def two = {
    val replaceTabsWithSpaces = "(\t)+".r
    val replaced: String = replaceTabsWithSpaces.replaceAllIn("c\tb\ta", "-")
    println("replaced = " + replaced)
  }

  def three = {
    println("(\\d){12,}".r.findAllIn(Source.fromFile("src/main/resources/9/3.txt", "UTF-8").mkString).mkString(" "))
  }

  def four = {
    val tokens: Array[String] = Source.fromFile("src/main/resources/9/4.txt", "UTF-8").mkString.split("\\s+")
    val numbers: Array[Double] = tokens.map(_.toDouble)
    println(numbers.max)
    println(numbers.min)
    println(numbers.sum)
    println(numbers.sum / numbers.length)
  }

  def nine = {
    subdirs(new File("/"))
    def subdirs(dir: File): Iterator[File] = {
      val children = dir.listFiles.filter(_.isDirectory)
      children.map(f => println(f))
      children.toIterator ++ children.toIterator.flatMap(subdirs _)
    }
  }

  def ten = {
    @SerialVersionUID(42L)
    case class Person(name: String) extends Serializable

    val fileName: String = "src/main/resources/9/10.obj"
    val out: ObjectOutputStream = new ObjectOutputStream(new FileOutputStream(fileName))
    out.writeObject(new Person("name"))
    out.close

    val in: ObjectInputStream = new ObjectInputStream(new FileInputStream(fileName))
    val person: Person = in.readObject().asInstanceOf[Person]
    println("person = " + person)
  }
}
