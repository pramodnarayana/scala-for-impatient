import org.scalatest.{Matchers, WordSpec}

import scala.collection.mutable.ArrayBuffer

class ChapterThreeTest extends WordSpec with Matchers {
  val chapterThree = new ChapterThree

  "Task 2" when {
    "leave empty array empty " in {
      // when
      val result: Array[Int] = chapterThree.two(Array.empty[Int])

      // then
      assert(result === Array.empty[Int])
    }
    "leave one element in array" in {
      // when
      val result: Array[Int] = chapterThree.two(Array[Int](1))

      // then
      result should be(Array[Int](1))
    }

    "reverse two elements in array" in {
      // when
      val result: Array[Int] = chapterThree.two(Array[Int](1, 2))

      // then
      assert(result === Array[Int](2, 1))
    }

    "reverse all elements in array" in {
      // given
      val in: Array[Int] = Array[Int](1, 2, 3, 4, 5)
      val expectedOut: Array[Int] = Array[Int](2, 1, 4, 3, 5)

      // when
      var result: Array[Int] = chapterThree.two(in)

      // then
      assert(expectedOut === result)
    }
  }
  "Task 3" when {
    "leave array empty" in {
      // when
      val result: Array[Int] = chapterThree.three(Array.empty[Int])

      // then
      assert(result === Array.empty[Int])
    }
    "reverse two elements in array" in {
      // when
      val result: Array[Int] = chapterThree.two(Array[Int](1, 2))

      // then
      assert(result === Array[Int](2, 1))
    }

    "reverse all elements in array" in {
      // given
      val in: Array[Int] = Array[Int](1, 2, 3, 4, 5)
      val expectedOut: Array[Int] = Array[Int](2, 1, 4, 3, 5)

      // when
      var result: Array[Int] = chapterThree.two(in)

      // then
      assert(expectedOut === result)
    }
  }
  "Task 4" when {
    "empty" should {
      "return empty array" in {
        // when
        val result: Array[Int] = chapterThree.four(Array.empty[Int])

        // then
        assert(Array.empty[Int] === result)
      }
    }
    "one element" should {
      "return single element array" in {
        // given
        val in: Array[Int] = Array[Int](1)

        // when
        val result: Array[Int] = chapterThree.four(in)

        // then
        assert(in === result)
      }
    }
    "two positive elements" should {
      "not change position" in {
        // given
        val in: Array[Int] = Array[Int](1, 2)

        // when
        val result: Array[Int] = chapterThree.four(in)

        // then
        assert(in === result)
      }
    }
    "one element positive" should {
      "positive comes first" in {
        // given
        val in: Array[Int] = Array[Int](-1, 1)

        // when
        val result: Array[Int] = chapterThree.four(in)

        // then
        assert(Array[Int](1, -1) === result)
      }
    }
    "several posities and several negatives" should {
      "positives comes before negatives" in {
        // given
        val in: Array[Int] = Array[Int](-1, 10, 0, -100, 1)

        // when
        val result: Array[Int] = chapterThree.four(in)

        // then
        assert(Array[Int](10, 1, -1, 0, -100) === result)
      }
    }
  }
  "Task 5" when {
    "iterative" should {
      "empty" should {
        "return 0" in {
          // when
          val result: Double = chapterThree.five(Array.empty[Double])

          // then
          assert(0.0 === result)
        }
      }
      "several elements" should {
        "return avg" in {
          // when
          val result: Double = chapterThree.five(Array[Double](1.0, 2.0, 3.0))

          // then
          result should equal(2.0 +- 0.1)
        }
      }
    }
    "recursive" should {
      "empty" should {
        "return 0" in {
          // when
          val result: Double = chapterThree.five_recursive(Array.empty[Double])

          // then
          assert(0.0 === result)
        }
      }
      "several elements" should {
        "return avg" in {
          // when
          val result: Double = chapterThree.five_recursive(Array[Double](1.0, 2.0, 3.0))

          // then
          result should equal(2.0 +- 0.1)
        }
      }
    }
  }
  "Task 6" when {
    "array" should {
      "several elements" should {
        "be sorted and reversed" in {
          // when
          val result: Array[Int] = chapterThree.six(Array[Int](1, 4, 2, 5, 3))

          // then
          result should equal(Array[Int](5, 4, 3, 2, 1))
        }
      }
    }
    "arraybuffer" should {
      "several elements" should {
        "be sorted and reversed" in {
          // when
          val result: ArrayBuffer[Int] = chapterThree.six(ArrayBuffer[Int](1, 4, 2, 5, 3))

          // then
          result should equal(ArrayBuffer[Int](5, 4, 3, 2, 1))
        }
      }
    }
    "Task 7" when {
      "simple" should {
        "nonrepeating elements not are present" should {
          "empty array" in {
            // given
            val in: Array[Int] = Array[Int](1, 1, 2, 2)

            // when
            val result: Array[Int] = chapterThree.seven(in)

            // then
            result should equal(Array.empty[Int])
          }
        }
        "one element" should {
          "stay one element" in {
            // given
            val in: Array[Int] = Array[Int](1)

            // when
            val result: Array[Int] = chapterThree.seven(in)

            // then
            result should equal(Array[Int](1))
          }
        }
        "nonrepeating elements are present" should {
          "elements which are nonrepeating" in {
            // given
            val in: Array[Int] = Array[Int](1, 1, 3, 2, 2, 5)

            // when
            val result: Array[Int] = chapterThree.seven(in)

            // then
            result should equal(Array[Int](3, 5))
          }
        }
      }
      "with map" should {
        "nonrepeating elements not are present" should {
          "empty array" in {
            // given
            val in: Array[Int] = Array[Int](1, 1, 2, 2)

            // when
            val result: Array[Int] = chapterThree.seven_groupedBy(in)

            // then
            result should equal(Array.empty[Int])
          }
        }
        "one element" should {
          "stay one element" in {
            // given
            val in: Array[Int] = Array[Int](1)

            // when
            val result: Array[Int] = chapterThree.seven_groupedBy(in)

            // then
            result should equal(Array[Int](1))
          }
        }
        "nonrepeating elements are present" should {
          "elements which are nonrepeating" in {
            // given
            val in: Array[Int] = Array[Int](1, 1, 3, 2, 2, 5)

            // when
            val result: Array[Int] = chapterThree.seven_groupedBy(in)

            // then
            result should equal(Array[Int](3, 5))
          }
        }
      }
    }
  }
}
