import scala.collection.mutable.ArrayBuffer
import scala.util.Sorting

val ints: Array[Int] = Array[Int](1, 2, 3, 4, 5)
val result: ArrayBuffer[Int] = ArrayBuffer[Int]()
def method {
  for (i <- (0 until ints.length) if i % 2 == 0 && i < ints.length - 1) {
    result += ints(i + 1)
    result += ints(i)
  }
}
method
result


